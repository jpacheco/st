static const char *colorname[] = {
  "#000000", 
  "#e06c75", 
  "#98c379", 
  "#e5c07b", 
  "#61afef", 
  "#c678dd", 
  "#56b6c2", 
  "#abb2bf", 
  "#545862", 
  "#d19a66", 
  "#353b45", 
  "#3e4451", 
  "#565c64", 
  "#b6bdca", 
  "#be5046", 
  "#c8ccd4", 

[256] = "#CDD6F4", 
[257] = "#000000", 
[258] = "#F5E0DC", 

};

unsigned int defaultfg = 256;
unsigned int defaultbg = 257;
unsigned int defaultcs = 258;
static unsigned int defaultrcs = 258;
